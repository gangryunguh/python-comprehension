# List comprehensions can be used whenever you see a "for loop" that
# loops over an iterable, transforming each item and adding it to a list


# 1. Starting with a vowel [9 points]
def vowel_names(names: object) -> object:
    """
    [ pts] Make a function that accepts a list of names and returns a
    new list containing all names that start with a vowel
    :param names: list of names
    :return: list containing all names that start with a vowel
    """
    return [name for name in names if name[0].lower() in ('a', 'e', 'i', 'o', 'u')]


# 2. Power List By Index [9 points]
def power_list(nums):
    """
    Make a function that accepts a list of numbers and returns a
    new list containing each number raised to the i-th power where
    i is the index of that number in the given list.
    :param nums: list of numbers
    :return: list of numbers where each number is raised to the i-th power
             where i is the index of that number in the given list
    """
    return [number ** i for i, number in enumerate(nums)]


# 3. Divisible [9 points]
def divisible_by_6_and_10(start, end):
    """
    Make a function  that returns a list of all numbers between start and end
    that are divisible by 6 and 10
    :param start:
    :param end:
    :return: list of numbers that a divisible by 6 and 10
    """
    return [n for n in range(start, end + 1) if n % 6 == 0 and n % 10 == 0]


# 4. Flatten a Matrix [10 points]
def flatten(matrix):
    """
    Make a function that takes a matrix (a list of lists) and
    return a flattened version of the matrix.
    :param matrix:
    :return: vector (list) of flattened matrix
    """
    return [item for row in matrix for item in row]


# 5. Secret Message [10 points]
def secret_message(str):
    """
    Get a list of all uppercase letters in str and find a secret 4 letter word
    in this uppercase letter list. The word starts at index 374 ends at index 382,
    and skips every other letter
    :param str:
    :return: secret_word
    """
    capitals = [c for c in str if c.isupper()]
    secret_word = "".join(capitals[374:382:2])
    return secret_word


# 6. ASCII Strings [10 points]
def get_ascii_codes(words):
    """
    Accepts a list of strings and returns a dictionary containing the strings
    as keys and a list of corresponding ASCII character codes as values.
    :param words: set of strings
    :return dictionary of tuples (string: list of numeric keys)
    """
    return {name: [ord(k) for k in name] for name in words}


# 7. Factors [10 points]
def get_all_factors(numbers):
    """
    takes a set of numbers and makes a dictionary containing the numbers
    as keys and all factors as values.
    :param numbers: list of numbers
    :return dictionary of tuples (number: list of factors)
    """
    return {number: [i for i in range(1, number + 1) if number % i == 0] for number in numbers}


# 8. Flipped Dictionary [8 points]
def flip_dict(adict):
    """
    flips dictionary keys and values.
    :param adict: dictionary of tuples (key,value)
    :return dictionary of tuples (value, key)
    """
    return {value:key for key,value in adict.items()}


# 9. Most common [15 points]
def most_common(*isets):
    """
    accepts any number of sets and returns a set of the most common items
    from each of the given sets.
    :param isets:
    :return oset: a set of the most common items from each of the given sets.
    """
    items = [ item for iset in isets for item in iset ]
    unique_items = set(items)
    occurrences = {}
    for s in unique_items:
        for i in items:
            occurrences.setdefault(i,0)
            occurrences[i] += 1
    most = max(occurrences.values())
    return {i for i,count in occurrences.items() if count == most}


# 10. X marks the spot [10 points]
def x_set(lines):
    """
    Find all words in lines that contain the letter “x”
    :param lines: list of lines - each line is a string
    :return: set of words where each word contains the letter 'x' or 'X'
    """
    items = [item for line in data for item in line.split()]
    return {w for w in items if 'x' in w.lower()}


if __name__ == "__main__":
    total = 0
    """ test for vowel_names """
    names = ["Alice", "Bob", "Christy", "Jules", "Umbrella"]
    answer = ["Alice", "Umbrella"]
    v_names = vowel_names(names)
    print("test1: vowel_names() correct? : {}".format(v_names == answer))
    if v_names == answer:
        total += 9

    """ test for power_list"""
    numbers = [78, 700, 82, 16, 2, 3, 9.5]
    answer = [1, 700, 6724, 4096, 16, 243, 735091.890625]
    v_numbers = power_list(numbers)
    print("test2: power_list() correct? : {}".format(v_numbers == answer))
    if v_numbers == answer:
        total += 9

    """ test for divisible_by_6_and_10 """
    answer = [120, 150, 180, 210, 240, 270, 300]
    v_numbers = divisible_by_6_and_10(100, 300)
    print("test3: divisible_by_6_and_10() correct? : {}".format(v_numbers == answer))
    if v_numbers == answer:
        total += 9

    """ test for flatten """
    matrix = [[row * 3 + i for i in range(1, 4)] for row in range(4)]
    answer = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    v_numbers = flatten(matrix)
    print("test4: flatten() correct? : {}".format(v_numbers == answer))
    if v_numbers == answer:
        total += 10

    """ test for secret message """
    with open("declaration-of-independence.txt", "r") as infile:
        data = infile.readlines()
    str = "".join(data)
    answer = "NEED"
    v_str = secret_message(str)
    print("test5: secret_message() correct? : {}".format(answer == v_str))
    if v_str == answer:
        total += 10

    """ test for get_ascii_codes """
    words = ["hello", "bye", "yes", "no", "python"]
    answer = {'yes': [121, 101, 115], 'hello': [104, 101, 108, 108, 111],
              'python': [112, 121, 116, 104, 111, 110], 'no': [110, 111],
              'bye': [98, 121, 101]}
    v_dict = get_ascii_codes(words)
    print("test6: get_ascii_codes() correct? : {}".format(answer == v_dict))
    if v_dict == answer:
        total += 10

    """ test for get_all_factors """
    numbers = {62, 293, 314}
    answer = {314: [1, 2, 157, 314], 293: [1, 293], 62: [1, 2, 31, 62]}
    v_dict = get_all_factors(numbers)
    print("test7: get_all_factors() correct? : {}".format(answer == v_dict))
    if v_dict == answer:
        total += 10

    """ test for flip_dictionary """
    answer = {'2015-09-13': 'C', '2015-09-15': 'Python', '2015-09-14': 'Java'}
    v_dict = flip_dict({'Python': "2015-09-15", 'Java': "2015-09-14", 'C': "2015-09-13"})
    print("test8: flip_dict() correct? : {}".format(answer == v_dict))
    if v_dict == answer:
        total += 8

    """ test for most_common """
    restaurants_trey = {'Habaneros', 'Karl Strauss', 'Opera', 'Punjabi Tandoor'}
    restaurants_diane = {'Siam Nara', 'Karl Strauss', 'Punjabi Tandoor', 'Opera'}
    restaurants_peter = {'Karl Strauss', 'Opera', 'Habaneros'}
    answer = {'Karl Strauss', 'Opera'}
    v_set = most_common(restaurants_trey, restaurants_diane, restaurants_peter)
    print("test9: most_common() correct? : {}".format(answer == v_set))
    if v_set == answer:
        total += 15

    """ test for X marks the spot """
    with open("declaration-of-independence.txt", "r") as infile:
        data = infile.readlines()
    answer = {'Sexes', 'Executioners', 'Experience', 'exercise;', 'Taxes', 'extend',
              'Example', 'excited', 'exposed'}
    v_set = x_set(data)
    print("test10: x_set() correct? : {}".format(answer == v_set))
    if v_set == answer:
        total += 10
    print("------------------------------------------------------")
    print("Your score = {}/100".format(total))
